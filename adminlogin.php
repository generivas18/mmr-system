<?php 
require_once("include/conn.php");
if(isset($_SESSION['admin_uid'])){
  header("Location: admindashboard.php");
}
else {
    session_start();
}

if(isset($_POST["loginaadmin"])){

    $myusername = $_POST['username'];
    $mypassword = $_POST['adminpassword'];
  
    $query  = "SELECT * from admin where username = 
    '$myusername' and password = '$mypassword' ";
  
    $result = mysqli_query($connection, $query);
    $count = mysqli_num_rows($result);
    $login = mysqli_fetch_assoc($result); 
  
    if($count >= 1) {
      session_start(); 
      $_SESSION['logined_adminid'] = $login['admin_id'];
      $_SESSION['admin_uid'] = $login['admin_id'];
      header("Location: admindashboard.php");
    }

    else{
      echo "<script type='text/javascript'>alert('Invalid Credentials!')</script>";
      echo "<script>window.location='adminlogin.php';</script>";
      echo "<script>close()</script>";
    }
  }  

if(isset($_SESSION['logined_adminid'])) {
    header("Location: admindashboard.php");}
?>
<html>
<head>
  
  <!--Import Google Icon Font-->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!--Import materialize.css-->
  <title>SERDAC System</title>
  <link type="text/css" rel="stylesheet" href="css/stylesheet.css" />
  <link type="text/css" rel="stylesheet" href="css/materialize.css" media="screen,projection" />

  <!--Let browser know website is optimized for mobile-->
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="icon" href="images/favicon.ico" type="image/ico" sizes="16x16">
</head>
<body>
 <div class="container">
      <div class="row">
        <!--login and registration buttons-->
        <div class="col s12 center-align">
        <h4>ADMIN LOGIN</h4>
              <div class="container">
                <form action="" method="POST">
                  <div class="row">
                    <div class="col s12">
                      <div class="row">
                        <div class="input-field offset-s1 col s10">
                          <input id="username" name="username" type="text" class="validate" required>
                          <label for="username">Username</label>
                        </div>
                      </div>
                      <div class="row">
                        <div class="input-field offset-s1 col s10">
                          <input id="adminpassword" name="adminpassword" type="password" class="validate" required>
                          <label for="adminpassword">Password</label>
                        </div>
                      </div>
                      <div class="row">
                        <button class="btn-large blue" type="submit" name="loginaadmin">Login</button>
                      </div>
                    </div>
                  </div>
                  </form>
              </div>    
            </div>
      </div>
  </div>
</body>
<script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
</html>  