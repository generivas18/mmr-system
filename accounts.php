<?php
 include("include/session_admin.php");
 require_once("include/conn.php");
 $uid = $_SESSION['admin_uid'];
 $tosearch = $_GET['tosearch'];

 if($tosearch==''){
    echo "<script>window.location='admindashboard.php';</script>";
    echo "<script>close()</script>";
}
 if($tosearch=='all'){
     $tosearch = '';
 }

 if(isset($_POST["search_Go"])){
    $tosearch2 = $_POST["tosearch"];
    echo "<script>window.location='accounts.php?tosearch=$tosearch2';</script>";
    echo "<script>close()</script>";
 }
 

 if(isset($_POST["register_Go"])){
    $firstname = $_POST["firstname"];
    $lastname = $_POST["lastname"];
    $id_number = $_POST["id_number"];
    $school = $_POST["school"];
    $course = $_POST["course"];
    $contact_number = $_POST["contact_number"];
    $designation = $_POST["designation"];
    
    $genuser_id = "Select coalesce(max(user_id), 0) + 1 as user_id from basic_user_info";
    $q = mysqli_query($connection, $genuser_id);
    $row_genid = mysqli_fetch_assoc($q);
    $user_id = $row_genid['user_id'];
    $firstname = mysqli_real_escape_string($connection, $firstname);
    $lastname = mysqli_real_escape_string($connection, $lastname);
    $id_number = mysqli_real_escape_string($connection, $id_number);
    $school = mysqli_real_escape_string($connection, $school);
    $course = mysqli_real_escape_string($connection, $course);
    $contact_number = mysqli_real_escape_string($connection, $contact_number);
    $designation = mysqli_real_escape_string($connection, $designation);

    $query_11 = "SELECT * FROM basic_user_info WHERE id_number='$id_number'";
		$result1 = mysqli_query($connection, $query_11);
    if(mysqli_num_rows($result1) >= 1){
        echo "<script type='text/javascript'>alert('ID Number Already Taken!!')</script>";
    }
    else{
        $query_basicinfo = mysqli_query($connection, "INSERT INTO basic_user_info (user_id, id_number, firstname, lastname, date_joined, school, course, contact_number, designation) VALUES ($user_id, '$id_number','$firstname','$lastname', CURRENT_TIMESTAMP, '$school', '$course', '$contact_number', '$designation')");
        
        if($query_basicinfo){
                echo "<script type='text/javascript'>alert('Registration Successfull!')</script>";
                echo "<script>window.location='accounts.php?tosearch=all';</script>";
                echo "<script>close()</script>";
        }
        else{
            echo "<script type='text/javascript'>alert('Registration Failed!')</script>";
            echo "<script>window.location='accounts.php?tosearch=all';</script>";
            echo "<script>close()</script>";
         }
    }
 }
?>  

<html>
  <head>
      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <title>SERDAC System</title>
      <link type="text/css" rel="stylesheet" href="css/stylesheet.css" />
      <link type="text/css" rel="stylesheet" href="css/materialize.css" media="screen,projection" />

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <link rel="icon" href="images/favicon.ico" type="image/ico" sizes="16x16">
      <div class="navbar-fixed">
            <nav>
                <div class="nav-wrapper blue row">
                <a href="admindashboard.php" class="brand-logo left">SERDAC</a>
                    <div class="container">
                            <ul id="nav-mobile" class="left hide-on-med-and-down">
                                <li><a href="logs.php?tosort=all">Logs</a></li>
                                <li><a href="resources.php">Resources</a></li>
                                <li><a href="services.php">Services</a></li>
                                <li class="active"><a href="accounts.php?tosearch=all">Accounts</a><li>
                            </ul>
                    </div>
                    <ul id="nav-mobile" class="right hide-on-med-and-down">
                        <a href="include/logout_admin.php">Logout</a>
                    </ul>   
                </div>
            </nav>
        </div>
  </head>
<body>

    <div class="container">
      <div class="row">
        <!--login and registration buttons-->
        <div class="col s12 center-align">        
              <div id="tab2">
                <div class="row">
                 <h4 class="left-align"><u>All Accounts</u>
                    <a class="btn-floating btn-medium blue modal-trigger btn tooltipped" data-position="right" data-tooltip="Add Accounts" href="#addAccount"><i class="material-icons">add</i></a>
                    <a class="btn-floating btn-medium blue modal-trigger btn tooltipped right hide-on-med-and-down" data-position="right" data-tooltip="Search" href="#search"><i class="material-icons">search</i></a>
                    <input class="col s3 right hide-on-med-and-down" disabled value="<?php echo $tosearch?>" id="disabled" type="text" >
                   
                </h4>
                            <table class="centered">
                                        <thead>
                                        <?php 
                                                if($tosearch == ''){
                                                    $query_logs3  = "SELECT * FROM basic_user_info";
                                                    $results_logs3 = mysqli_query($connection, $query_logs3);
                                                }
                                                else {
                                                    $query_logs3  = "SELECT * FROM basic_user_info where firstname LIKE '%$tosearch%' or lastname LIKE '%$tosearch%' or id_number LIKE '%$tosearch%'";
                                                    $results_logs3 = mysqli_query($connection, $query_logs3);
                                                }
                                               
                                                if(mysqli_num_rows($results_logs3) < 1){
                                                    ?> <h4>No Accounts Found</h4><?php
                                                }
                                                else{
                                        ?>
                                            <tr>
                                                <th>ID Number</th>
                                                <th>Name</th>
                                                <th>School</th>
                                                <th>Course</th>
                                                <th>Designation</th>
                                                <th>View</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                                while($results3 = mysqli_fetch_assoc($results_logs3)){
                                            ?>
                                            <tr>
                                                <td><?php echo $results3['id_number'] ?></td>
                                                <td><?php echo $results3['firstname'].' '.$results3['lastname'] ?></td>
                                                <td><?php echo $results3['school'] ?></td>
                                                <td><?php echo $results3['course'] ?></td>
                                                <td><?php echo $results3['designation'] ?></td>
                                                <td>
                                                    <a name="toview" href="optionsAccounts.php?number=<?php echo $results3['user_id'];?>">View
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php }} ?>
                                        </tbody>
                                    </table> 
                                    <?php 
                                    //storedProcedure
                                    $queryProcedure1 = "CALL `getAccounts_count`()";//stored procedure
                                    $resultProcedure = mysqli_query($connection, $queryProcedure1);
                                    $rowProc = mysqli_fetch_assoc($resultProcedure);
                                    ?>
                                    <h5 class="right-align">
                                    <button class="btn-floating blue tooltipped" data-position="left" data-tooltip="Number of Accounts"><i class="material-icons">people</i></button>
                                  
                                    <?php
                                        echo $rowProc['count'];
                                    ?>   </h5>          
                </div>   
        </div>
      </div>
    </div> 
    <div id="addAccount" class="modal">
              <div class="modal-content">
              <div class="center-align">
              <h4>Add Account</h4>
                <form action="" method="POST">
                <div class="row">
                        <div class="input-field col s6">
                            <input id="firstname" name="firstname" type="text" class="validate" required>
                            <label for="firstname">First Name</label>
                        </div>
                        <div class="input-field col s6">
                            <input id="lastname" name="lastname" type="text" class="validate" required>
                            <label for="lastname">Last Name</label>
                        </div>        
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" name="id_number" id="id_number" class="validate" required>
                            <label for="id_number">ID Number</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" name="school" id="school" class="validate" required>
                            <label for="school">School</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" name="course" id="course" class="validate" required>
                            <label for="course">Course</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" name="contact_number" id="contact_number" class="validate" required>
                            <label for="contact_number">Contact Number</label>
                        </div>       
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" name="designation" id="designation" class="validate" required>
                            <label for="designation">Designation</label>
                        </div>       
                    </div>
                    <div class="row center-align">
                            <button class="btn-large blue" type="submit" name="register_Go">Register</button>
                    </div>  
                </form>
            
          </div>
        </div>
    </div>  
    <div id="search" class="modal">
              <div class="modal-content">
              <div class="center-align">
              <div class="col s12">
              <h4>Search</h4>
                <form action="" method="POST">
                        <div class="input-field col s6">
                            <input type="text" name="tosearch" id="tosearch" class="validate" value="<?php echo $tosearch?>" required>
                            <label for="tosearch">Name or IDNumber</label>
                        </div>      
                <div class="row">
                            <button class="btn-large blue" type="submit" name="search_Go">Search</button>
                            <a class="btn-large blue" href="accounts.php?tosearch=all">View All<a>
               
                </div>  
                </form>
                </div>
                </div>
        </div>
    </div>
    

    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <script>$(document).ready(function(){
    $('select').formSelect();
  });

    var elem1 = document.querySelector('#addAccount');
    var instance1 = M.Modal.init(elem1);

     var elem2 = document.querySelector('#search');
    var instance2 = M.Modal.init(elem2);

    $(document).ready(function(){
    $('.tooltipped').tooltip();
  });
    </script>

      <script>
    $(document).ready(function(){
         $('.tabs').tabs();
    });</script>
</body>
</html>