<?php
 include("include/session_admin.php");
 require_once("include/conn.php");
 $uid = $_SESSION['admin_uid'];
 
  $query_2 = "SELECT * FROM service";
  $result_2 = mysqli_query($connection, $query_2);
  if(isset($_POST["availService"])){
    $user_id = $_POST["name"];
    $service_id = $_POST["servicedata"];
    $remarks = $_POST["remarks"];
    $extrafee = $_POST["extrafee"];
    $query_13 = "SELECT * FROM services where service_id = $service_id";
    $result_13 = mysqli_query($connection, $query_13);
    $row_13 = mysqli_fetch_assoc($result_13);
    $totalfee = $row_13['fixed_rate'];

    $user_id = mysqli_real_escape_string($connection, $user_id);
    $service_id = mysqli_real_escape_string($connection, $service_id);
    $totalfee = mysqli_real_escape_string($connection, $totalfee);
    $remarks = mysqli_real_escape_string($connection, $remarks);
    $extrafee = mysqli_real_escape_string($connection, $extrafee);

    if($extrafee == ''){
        $extrafee = 0;
    }
    $genser_idquery = "Select coalesce(max(service_transaction_id), 0) + 1 as service_transaction_id from service_transactions";
    $g2 = mysqli_query($connection, $genser_idquery);
    $row_genserid = mysqli_fetch_assoc($g2);
    $genser_id = $row_genserid['service_transaction_id'];
    $query_genst= mysqli_query($connection, "INSERT INTO service_transactions (service_transaction_id, service_id, total_fee, extra_fee, name, admin_id, st_datetime, remarks) VALUES ($genser_id, $service_id, $totalfee, $extrafee, '$user_id', $uid, CURRENT_TIMESTAMP, '$remarks')");
    if($query_genst){
        $genser_idquerylog = "Select coalesce(max(log_id), 0) + 1 as log_id from logs";
        $g3 = mysqli_query($connection, $genser_idquerylog);
        $row_genserlog = mysqli_fetch_assoc($g3);
        $genser_logid = $row_genserlog['log_id'];
        $query_ser_logs = mysqli_query($connection, "INSERT INTO logs (log_id, log_type, log_time, service_transaction_id) VALUES ($genser_logid,'service_purchase', CURRENT_TIMESTAMP,  $genser_id)");
        if($query_ser_logs){
            echo "<script type='text/javascript'>alert('Service Purchased Successfully')</script>";
            echo "<script>window.location='logs.php?tosort=all#tab4';</script>";
            echo "<script>close()</script>";
        }
    }   
 }
 if(isset($_POST["add_Go"])){
    $service_name = $_POST["service_name"];
    $service_details = $_POST["service_details"];
    $fixed_rate = $_POST["fixed_rate"];

    $service_name = mysqli_real_escape_string($connection, $service_name);
    $service_details = mysqli_real_escape_string($connection, $service_details);
    $fixed_rate = mysqli_real_escape_string($connection, $fixed_rate);

    $genser_idquery = "Select coalesce(max(service_id), 0) + 1 as service_id from services";
    $g2 = mysqli_query($connection, $genser_idquery);
    $row_genserid = mysqli_fetch_assoc($g2);
    $genser_id = $row_genserid['service_id'];
    $query_genservice= mysqli_query($connection, "INSERT INTO services (service_id, service_name, service_details, fixed_rate) VALUES ($genser_id, '$service_name', '$service_details', $fixed_rate)");
        if($query_genservice){
        echo "<script type='text/javascript'>alert('Service Created Successfully')</script>";
        echo "<script>window.location='services.php';</script>";
        echo "<script>close()</script>";
        }
 }
?>  

<html>
  <head>
      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <title>SERDAC System</title>
      <link type="text/css" rel="stylesheet" href="css/stylesheet.css" />
      <link type="text/css" rel="stylesheet" href="css/materialize.css" media="screen,projection" />

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <link rel="icon" href="images/favicon.ico" type="image/ico" sizes="16x16">
      <div class="navbar-fixed">
            <nav>
                <div class="nav-wrapper blue row">
                <a href="admindashboard.php" class="brand-logo left">SERDAC</a>
                    <div class="container">
                            <ul id="nav-mobile" class="left hide-on-med-and-down">
                                <li><a href="logs.php?tosort=all">Logs</a></li>
                                <li><a href="resources.php">Resources</a></li>
                                <li class="active"><a href="services.php">Services</a></li>
                                <li><a href="accounts.php?tosearch=all">Accounts</a><li>
                            </ul>
                    </div>
                    <ul id="nav-mobile" class="right hide-on-med-and-down">
                        <a href="include/logout_admin.php">Logout</a>
                    </ul>   
                </div>
            </nav>
        </div>
  </head>
<body>

    <div class="container">
      <div class="row">
        <!--login and registration buttons-->
        <div class="col s12 center-align">
        
              <div class="container">
                    <ul class="tabs">
                               <li class="tab col s4 grey lighten-4"><a href="#tab1">Avail Service</a></li>
                               <li class="tab col s4 grey lighten-4"><a href="#tab2">View Services</a></li>
                    </ul>  
                    
                    <div id="tab1">
                    <h4 class="left-align"><u>Avail Service</u>
                            </h4> 
                        <form action="" method="POST">    
                           
                                <div class="input-field col s12">
                                <input name="name" id="name" type="text" class="validate" required>
                                <label for="name">Name</label>
                                </div>    
                        <div class ="input-field col s12">        
                            <select name="servicedata">
                                <?php 
                                     $query_ser = "SELECT * FROM services";
                                     $result_ser = mysqli_query($connection, $query_ser); 
                                    while($row_res = mysqli_fetch_assoc($result_ser)){
                                ?>
                                <option value="<?php echo $row_res['service_id'] ?>" ><?php echo $row_res['service_name']." (&#8369 ".number_format((float)$row_res['fixed_rate'], 2, '.', '').")"  ?></option><?php
                                    }
                                ?>
                            </select>
                        <label>Services</label>
                        </div>
                                <div class="input-field col s12">
                                    <input name="extrafee" id="extrafee" type="text" class="validate">
                                    <label for="extrafee">Additional Fee</label>
                                </div>
                                <div class="input-field col s12">
                                    <input name="remarks" id="remarks" type="text" class="validate">
                                    <label for="remarks">Remarks</label>
                                </div>
                    <button class="btn-large blue" type="submit" name="availService">Avail Service</button>
                    </form>
                </div>  
              <div id="tab2">
                 <h4 class="left-align">View Services
                            <a class="btn-floating btn-medium blue modal-trigger right hide-on-med-and-down btn tooltipped" data-position="right" data-tooltip="Add Service" href="#addService"><i class="material-icons">add</i></a>
                            </h4>
                            <div id="welcomeDiv">
                            <table class="centered">
                                        <thead>
                                        <?php 
                                        //naa diri ang view
                                                $query_logs3  = "SELECT * FROM `services`";// service view
                                                $results_logs3 = mysqli_query($connection, $query_logs3);
                                                if(mysqli_num_rows($results_logs3) < 1){
                                                    ?> <h4>No Services</h4><?php
                                                }
                                                else{
                                        ?>
                                            <tr>
                                                <th>Service Name</th>
                                                <th>Service Fee</th>
                                                <th>Service Details</th>
                                                <th>Edit</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                                while($results3 = mysqli_fetch_assoc($results_logs3)){
                                            ?>
                                            <tr>
                                                <td><?php echo $results3['service_name'] ?></td>
                                                <td><?php echo $results3['fixed_rate'] ?></td>
                                                <td><?php echo $results3['service_details'] ?></td>
                                            
                                                <td>
                                                    <a name="toedit" href="optionsServices.php?number=<?php echo $results3['service_id'];?>">Edit
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php }} ?>
                                        </tbody>
                                    </table> 
                            </div>    
              </div>
            </div>   
        </div>
      </div>
    </div> 
    <div id="addService" class="modal">
              <div class="modal-content">
              <div class="center-align">
              <h4>Add Services</h4>
                <div class="container">
                <form action="" method="POST">
                <div class="row">
                        <div class="input-field col s12">
                            <input type="text" name="service_name" id="service_name" class="validate" >
                            <label for="service_name">Service Name</label>
                        </div>       
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" name="service_details" id="service_details" class="validate" >
                            <label for="service_details">Remarks</label>
                        </div>       
                    </div>
                    <div class="input-field col s12">
                            <input type="text" name="fixed_rate" id="fixed_rate" class="validate" >
                            <label for="fixed_rate">Service Fee</label>
                        </div>     
                    <div class="row center-align">
                            <button class="btn-large blue" type="submit" name="add_Go">Add Service</button>
                    </div>
                </div>
                </form>
            </div>
          </div>
        </div>
    

    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <script>$(document).ready(function(){
    $('select').formSelect();
  });

    var elem1 = document.querySelector('#addService');
    var instance1 = M.Modal.init(elem1);

    $(document).ready(function(){
    $('.tooltipped').tooltip();
  });
    </script>

      <script>
    $(document).ready(function(){
         $('.tabs').tabs();
    });</script>
</body>
</html>