
<script type='text/javascript'>
    window.onload = function() { BindEvent(); }

    function BindEvent()
    {
        var elemToBind = document.getElementById ( "cmb1" );
        elemToBind.onchange = function () { SetSel ( this ); }
    }
    function SetSel(elem)
    {
        var secondCombo = document.getElementById ( "cmb2" );
        secondCombo.value = elem.value;   
    }
</script>
<body>
    <select id="cmb1">
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
    </select>
    <select id="cmb2">
        <option value="1">1a</option>
        <option value="2">2a</option>
        <option value="3">3a</option>
        <option value="4">4a</option>
    </select>
</body>