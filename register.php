<?php
require_once("include/conn.php");
/*
if(isset($_SESSION['uid'])){
    header("Location: dashboard.php");
  } else {
      session_start();
  }  */
 if($_SERVER["REQUEST_METHOD"] == "POST") {
    $firstname = $_POST["firstname"];
    $lastname = $_POST["lastname"];
    $id_number = $_POST["id_number"];
    $school = $_POST["school"];
    $course = $_POST["course"];
    $contact_number = $_POST["contact_number"];
    $designation = $_POST["designation"];
    
    $genuser_id = "Select coalesce(max(user_id), 0) + 1 as user_id from basic_user_info";
    $q = mysqli_query($connection, $genuser_id);
    $row_genid = mysqli_fetch_assoc($q);
    $user_id = $row_genid['user_id'];
    $firstname = mysqli_real_escape_string($connection, $firstname);
    $lastname = mysqli_real_escape_string($connection, $lastname);
    $id_number = mysqli_real_escape_string($connection, $id_number);
    $school = mysqli_real_escape_string($connection, $school);
    $course = mysqli_real_escape_string($connection, $course);
    $contact_number = mysqli_real_escape_string($connection, $contact_number);
    $designation = mysqli_real_escape_string($connection, $designation);

    $query_11 = "SELECT * FROM basic_user_info WHERE id_number='$id_number'";
		$result1 = mysqli_query($connection, $query_11);
    if(mysqli_num_rows($result1) >= 1){
        echo "<script type='text/javascript'>alert('ID Number Already Taken!!')</script>";
    }
    else{
        $query_basicinfo = mysqli_query($connection, "INSERT INTO basic_user_info (user_id, id_number, firstname, lastname, date_joined, school, course, contact_number, designation) VALUES ($user_id, '$id_number','$firstname','$lastname', CURRENT_TIMESTAMP, '$school', '$course', '$contact_number', '$designation')");
        
        if($query_basicinfo){
                echo "<script type='text/javascript'>alert('Registration Successfull!')</script>";
                echo "<script>window.location='index.php';</script>";
                echo "<script>close()</script>";
        }
    }
}


 ?> 
<html>
  <head>

      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <title>SERDAC System</title>
      <link type="text/css" rel="stylesheet" href="css/stylesheet.css" />
      <link type="text/css" rel="stylesheet" href="css/materialize.css" media="screen,projection" />

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <link rel="icon" href="images/favicon.ico" type="image/ico" sizes="16x16">
  </head>
<h4 class="center-align">Register</h4>  
       <div class="container">
                  <form class="col s12" action="" method="post">
                    <div class="row">
                        <div class="input-field col s6">
                            <input id="first_name" name="firstname" type="text" class="validate" required>
                            <label for="first_name">First Name</label>
                        </div>
                        <div class="input-field col s6">
                            <input id="last_name" name="lastname" type="text" class="validate" required>
                            <label for="last_name">Last Name</label>
                        </div>        
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" name="id_number" id="id_number" class="validate" required>
                            <label for="id_number">ID Number</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" name="school" id="school" class="validate" required>
                            <label for="school">School</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" name="course" id="course" class="validate" required>
                            <label for="course">Course</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" name="contact_number" id="contact_number" class="validate" required>
                            <label for="contact_number">Contact Number</label>
                        </div>       
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" name="designation" id="designation" class="validate" required>
                            <label for="designation">Designation</label>
                        </div>       
                    </div>
                    <div class="row center-align">
                            <button class="btn-large blue" type="submit" name="register_Go">Register</button>
                    </div>
                </form>
                <p>
            <!--  <a class="waves-effect waves-light btn blue">Back to Login</a>-->
              </div>
             

<script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
</body>
</html>