-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jan 03, 2019 at 10:58 AM
-- Server version: 5.7.19
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `serdac_system`
--

DELIMITER $$
--
-- Procedures
--
DROP PROCEDURE IF EXISTS `getAccounts_count`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `getAccounts_count` ()  NO SQL
Select coalesce(max(user_id), 0) as count from basic_user_info$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
CREATE TABLE IF NOT EXISTS `admin` (
  `admin_id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(225) NOT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `username`, `password`) VALUES
(1, 'admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `basic_userinfo_history`
--

DROP TABLE IF EXISTS `basic_userinfo_history`;
CREATE TABLE IF NOT EXISTS `basic_userinfo_history` (
  `user_id` int(11) NOT NULL,
  `id_number` varchar(50) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `school` varchar(225) NOT NULL,
  `course` varchar(100) NOT NULL,
  `contact_number` varchar(20) NOT NULL,
  `designation` varchar(50) NOT NULL,
  `date_joined` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `basic_userinfo_history`
--

INSERT INTO `basic_userinfo_history` (`user_id`, `id_number`, `firstname`, `lastname`, `school`, `course`, `contact_number`, `designation`, `date_joined`) VALUES
(1, '2014-16099', 'Japheth', 'Domingo', 'Bangkal', 'Bachelor of Computing', '232393d', 'Student', '2018-12-17 00:55:51'),
(2, '2014-16202', 'Prince', 'Justin Mamelic', 'USEP', 'BSIT', '09212121', 'Students', '2018-12-19 01:26:57'),
(1, '2014-16099', 'gay', 'Domingo', 'Bangkal', 'Bachelor of Computing', '232393d', 'Student', '2018-12-17 00:55:51'),
(5, '2014-1100', 'Anti', 'Mage', 'Magic School', 'ManaKiller', '232323', 'Mid', '2018-12-08 16:00:00'),
(5, '2014-1100', 'Anti', 'Mage', 'Magic School', 'ManaKiller', '232323', 'Mid', '2018-12-08 16:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `basic_user_info`
--

DROP TABLE IF EXISTS `basic_user_info`;
CREATE TABLE IF NOT EXISTS `basic_user_info` (
  `user_id` int(11) NOT NULL,
  `id_number` varchar(50) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `school` varchar(225) NOT NULL,
  `course` varchar(100) NOT NULL,
  `contact_number` varchar(20) NOT NULL,
  `designation` varchar(50) NOT NULL,
  `date_joined` timestamp NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `basic_user_info`
--

INSERT INTO `basic_user_info` (`user_id`, `id_number`, `firstname`, `lastname`, `school`, `course`, `contact_number`, `designation`, `date_joined`) VALUES
(1, '2014-16099', 'Prince', 'Justin Mamelic', 'USEP', 'BSIT', '092131313', 'Student', '2019-01-02 14:18:22'),
(2, '2014-16202', 'Japheth', 'Domingo', 'USEP', 'BSIT', '092143832954', 'Student', '2019-01-03 02:26:38');

--
-- Triggers `basic_user_info`
--
DROP TRIGGER IF EXISTS `create_history`;
DELIMITER $$
CREATE TRIGGER `create_history` AFTER UPDATE ON `basic_user_info` FOR EACH ROW BEGIN
INSERT INTO basic_userinfo_history(user_id, id_number, firstname, lastname, school, course, contact_number, designation, date_joined)
VALUES(old.user_id, old.id_number, old.firstname, old.lastname, old.school, old.course, old.contact_number, old.designation, old.date_joined);
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `guest`
--

DROP TABLE IF EXISTS `guest`;
CREATE TABLE IF NOT EXISTS `guest` (
  `guest_id` int(11) NOT NULL,
  `guest_timestamp` timestamp NOT NULL,
  PRIMARY KEY (`guest_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

DROP TABLE IF EXISTS `logs`;
CREATE TABLE IF NOT EXISTS `logs` (
  `log_id` int(11) NOT NULL,
  `log_type` varchar(50) NOT NULL,
  `log_time` timestamp NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `guest_id` int(11) DEFAULT NULL,
  `resource_transaction_id` int(11) DEFAULT NULL,
  `service_transaction_id` int(11) DEFAULT NULL,
  KEY `user_id` (`user_id`),
  KEY `guest_id` (`guest_id`),
  KEY `resource_transaction_id` (`resource_transaction_id`),
  KEY `service_transaction_id` (`service_transaction_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`log_id`, `log_type`, `log_time`, `user_id`, `guest_id`, `resource_transaction_id`, `service_transaction_id`) VALUES
(1, 'service_purchase', '2019-01-02 14:26:44', NULL, NULL, NULL, 1),
(2, 'extra_service_charge', '2019-01-02 14:36:12', NULL, NULL, NULL, 1),
(3, 'service_purchase', '2019-01-02 17:03:33', NULL, NULL, NULL, 2),
(4, 'service_purchase', '2019-01-02 17:20:32', NULL, NULL, NULL, 3),
(5, 'extra_service_charge', '2019-01-02 17:20:48', NULL, NULL, NULL, 3),
(6, 'service_purchase', '2019-01-02 17:33:14', NULL, NULL, NULL, 4),
(7, 'extra_service_charge', '2019-01-02 19:06:40', NULL, NULL, NULL, 4),
(8, 'user_login', '2019-01-03 01:39:05', 1, NULL, NULL, NULL),
(9, 'user_logout', '2019-01-03 01:39:16', 1, NULL, NULL, NULL),
(12, 'user_login', '2019-01-03 02:28:21', 2, NULL, NULL, NULL),
(13, 'user_logout', '2019-01-03 02:28:51', 2, NULL, NULL, NULL),
(15, 'service_purchase', '2019-01-03 02:35:48', NULL, NULL, NULL, 5),
(16, 'service_purchase', '2019-01-03 03:41:45', NULL, NULL, NULL, 6),
(17, 'service_purchase', '2019-01-03 03:48:33', NULL, NULL, NULL, 7),
(18, 'service_purchase', '2019-01-03 05:20:18', NULL, NULL, NULL, 8),
(19, 'user_login', '2019-01-03 10:13:19', 1, NULL, NULL, NULL),
(20, 'user_logout', '2019-01-03 10:13:33', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `resource`
--

DROP TABLE IF EXISTS `resource`;
CREATE TABLE IF NOT EXISTS `resource` (
  `resource_id` int(11) NOT NULL,
  `resource_name` varchar(50) NOT NULL,
  `resource_details` varchar(225) NOT NULL,
  `resource_author` varchar(100) NOT NULL,
  `date_published` date DEFAULT NULL,
  PRIMARY KEY (`resource_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `resource`
--

INSERT INTO `resource` (`resource_id`, `resource_name`, `resource_details`, `resource_author`, `date_published`) VALUES
(1, 'Guiness', 'Guiness 2010', 'dfds', '2019-01-03'),
(2, 'ResNum1', 'Res3', 'Mame', '1991-02-06'),
(3, 'Alamat ni Johnny', 'The Story of a Man', 'Johnny Sims', '2015-06-10'),
(4, '\';oilyu5t45r4', 'fdsfds', 'ewrw', '2015-01-06');

-- --------------------------------------------------------

--
-- Table structure for table `resource_transactions`
--

DROP TABLE IF EXISTS `resource_transactions`;
CREATE TABLE IF NOT EXISTS `resource_transactions` (
  `resource_transaction_id` int(11) NOT NULL,
  `resource_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `guest_id` int(11) DEFAULT NULL,
  `name` varchar(150) DEFAULT NULL,
  `admin_id` int(11) NOT NULL,
  `rt_datetime` timestamp NOT NULL,
  PRIMARY KEY (`resource_transaction_id`),
  KEY `user_id` (`user_id`),
  KEY `resource_id` (`resource_id`),
  KEY `admin_id` (`admin_id`),
  KEY `guest_id` (`guest_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `resource_view`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `resource_view`;
CREATE TABLE IF NOT EXISTS `resource_view` (
`resource_id` int(11)
,`resource_name` varchar(50)
,`resource_details` varchar(225)
,`resource_author` varchar(100)
,`date_published` date
);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
CREATE TABLE IF NOT EXISTS `services` (
  `service_id` int(11) NOT NULL,
  `service_name` varchar(50) NOT NULL,
  `service_details` varchar(225) NOT NULL,
  `fixed_rate` float NOT NULL,
  PRIMARY KEY (`service_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`service_id`, `service_name`, `service_details`, `fixed_rate`) VALUES
(1, 'Clinic', 'Magpaclinic', 150.126),
(2, 'Consultation', 'Consult sa Veteranians', 143.32);

-- --------------------------------------------------------

--
-- Table structure for table `service_transactions`
--

DROP TABLE IF EXISTS `service_transactions`;
CREATE TABLE IF NOT EXISTS `service_transactions` (
  `service_transaction_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(150) DEFAULT NULL,
  `admin_id` int(11) NOT NULL,
  `total_fee` float NOT NULL,
  `extra_fee` float DEFAULT NULL,
  `remarks` varchar(200) DEFAULT NULL,
  `st_datetime` timestamp NOT NULL,
  PRIMARY KEY (`service_transaction_id`),
  KEY `user_id` (`user_id`),
  KEY `service_id` (`service_id`),
  KEY `admin_id` (`admin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `service_transactions`
--

INSERT INTO `service_transactions` (`service_transaction_id`, `service_id`, `user_id`, `name`, `admin_id`, `total_fee`, `extra_fee`, `remarks`, `st_datetime`) VALUES
(1, 2, 1, NULL, 1, 643.82, 500.5, NULL, '2019-01-02 14:26:44'),
(2, 1, 1, NULL, 1, 150.126, NULL, NULL, '2019-01-02 17:03:32'),
(3, 1, 1, NULL, 1, 250.126, 100, 'Magluto bukas', '2019-01-02 17:20:32'),
(4, 2, 1, NULL, 1, 468.66, 325.34, 'Easiest Money of my Life', '2019-01-02 17:33:14'),
(5, 2, 2, NULL, 1, 143.32, 100.5, 'Statement', '2019-01-03 02:35:48'),
(6, 2, 1, NULL, 1, 143.32, 0, '343', '2019-01-03 03:41:44'),
(7, 2, 1, NULL, 1, 143.32, 0, '', '2019-01-03 03:48:33'),
(8, 2, NULL, 'Barreto Boy', 1, 143.32, 0, 'Nagpaculta sa Iro', '2019-01-03 05:20:18');

-- --------------------------------------------------------

--
-- Structure for view `resource_view`
--
DROP TABLE IF EXISTS `resource_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `resource_view`  AS  select `resource`.`resource_id` AS `resource_id`,`resource`.`resource_name` AS `resource_name`,`resource`.`resource_details` AS `resource_details`,`resource`.`resource_author` AS `resource_author`,`resource`.`date_published` AS `date_published` from `resource` ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `logs`
--
ALTER TABLE `logs`
  ADD CONSTRAINT `logs_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `basic_user_info` (`user_id`),
  ADD CONSTRAINT `logs_ibfk_2` FOREIGN KEY (`guest_id`) REFERENCES `guest` (`guest_id`),
  ADD CONSTRAINT `logs_ibfk_3` FOREIGN KEY (`resource_transaction_id`) REFERENCES `resource_transactions` (`resource_transaction_id`),
  ADD CONSTRAINT `logs_ibfk_4` FOREIGN KEY (`service_transaction_id`) REFERENCES `service_transactions` (`service_transaction_id`);

--
-- Constraints for table `resource_transactions`
--
ALTER TABLE `resource_transactions`
  ADD CONSTRAINT `resource_transactions_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `basic_user_info` (`user_id`),
  ADD CONSTRAINT `resource_transactions_ibfk_2` FOREIGN KEY (`resource_id`) REFERENCES `resource` (`resource_id`),
  ADD CONSTRAINT `resource_transactions_ibfk_3` FOREIGN KEY (`admin_id`) REFERENCES `admin` (`admin_id`),
  ADD CONSTRAINT `resource_transactions_ibfk_4` FOREIGN KEY (`guest_id`) REFERENCES `guest` (`guest_id`);

--
-- Constraints for table `service_transactions`
--
ALTER TABLE `service_transactions`
  ADD CONSTRAINT `service_transactions_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `basic_user_info` (`user_id`),
  ADD CONSTRAINT `service_transactions_ibfk_2` FOREIGN KEY (`service_id`) REFERENCES `services` (`service_id`),
  ADD CONSTRAINT `service_transactions_ibfk_3` FOREIGN KEY (`admin_id`) REFERENCES `admin` (`admin_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
