<?php
 include("include/session_admin.php");
 require_once("include/conn.php");
 $uid = $_SESSION['admin_uid'];
 $var_value = $_GET['number'];

 $query_2 = "SELECT * FROM services where service_id = $var_value";
 $result_2 = mysqli_query($connection, $query_2);
 $countres = mysqli_num_rows($result_2);
 $showres = mysqli_fetch_assoc($result_2);

if($countres < 1){
    header("Location: services.php#tab2");
}

 if(isset($_POST["edit_Go"])){
    $service_name = $_POST["sname"];
    $service_details = $_POST["sdetails"];
    $service_price = $_POST["sprice"];

    $service_name = mysqli_real_escape_string($connection, $service_name);
    $service_details = mysqli_real_escape_string($connection, $service_details);
    $service_price = mysqli_real_escape_string($connection, $service_price);

    $query_updateservice= mysqli_query($connection, "UPDATE services SET service_name = '$service_name', service_details = '$service_details', fixed_rate = $service_price where service_id = $var_value");
        if($query_updateservice){
        echo "<script type='text/javascript'>alert('Service Updated Successfully')</script>";
        echo "<script>window.location='services.php#tab2';</script>";
        echo "<script>close()</script>";
        }
        else{
        echo "<script type='text/javascript'>alert('Service Update Failed!')</script>";
        echo "<script>window.location='optionsServices.php?number=$var_value';</script>";
        echo "<script>close()</script>";
        }
 }
?>  

<html>
  <head>
      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <title>SERDAC System</title>
      <link type="text/css" rel="stylesheet" href="css/stylesheet.css" />
      <link type="text/css" rel="stylesheet" href="css/materialize.css" media="screen,projection" />

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <link rel="icon" href="images/favicon.ico" type="image/ico" sizes="16x16">
      <div class="navbar-fixed">
            <nav>
                <div class="nav-wrapper blue row">
                <a href="admindashboard.php" class="brand-logo left">SERDAC</a>
                    <div class="container">
                            <ul id="nav-mobile" class="left hide-on-med-and-down">
                            </ul>
                    </div>
                    <ul id="nav-mobile" class="right hide-on-med-and-down">
                        <a href="include/logout_admin.php">Logout</a>
                    </ul>   
                </div>
            </nav>
        </div>
  </head>
<body>

    <div class="container">
      <div class="row">       
        <div class="col s12">
              <div class="container">
              <div class="center-align">
              <h4>
              
              <a href="services.php#tab2" class="btn-floating btn-medium blue left hide-on-med-and-down"><i class="material-icons">arrow_back</i></a>
              Edit Service</h4>
              
              <form action="" method="POST">
                  <div class="row">
                    <div class="col s12">
                      <div class="row">
                        <div class="input-field offset-s1 col s10">
                          <input id="sname" name="sname" type="text" class="validate" value="<?php echo $showres["service_name"] ?>" required>
                          <label for="sname">Service Name</label>
                        </div>
                      </div>
                      <div class="row">
                        <div class="input-field offset-s1 col s10">
                          <input id="sdetails" name="sdetails" type="text" class="validate" value="<?php echo $showres["service_details"] ?>" required>
                          <label for="sdetails">Remarks</label>
                        </div>
                      </div>
                      <div class="row">
                        <div class="input-field offset-s1 col s10">
                          <input id="sprice" name="sprice" type="text" class="validate" value="<?php echo $showres["fixed_rate"] ?>" required>
                          <label for="sprice">Service Price</label>
                        </div>
                      </div>
                      <div class="row">
                     
                        <button class="btn-large blue" type="submit" name="edit_Go">Edit</button>
                       </div>
                    </div>
                  </div>
                  </div> 
                </form>
              </div>
            </div>     
      </div>
    </div> 
</body>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/materialize.min.js">
    </script>
    <script>

    $(document).ready(function(){
    $('.tooltipped').tooltip();
  });

    </script>
  
</html>