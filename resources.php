<?php
 include("include/session_admin.php");
 require_once("include/conn.php");
 $uid = $_SESSION['admin_uid'];

 if(isset($_POST["add_Go"])){
    $resource_name = $_POST["resource_name"];
    $resource_details = $_POST["resource_details"];
    $author = $_POST["author"];
    $datepub = $_POST["datepub"];

    $resource_name = mysqli_real_escape_string($connection, $resource_name);
    $resource_details = mysqli_real_escape_string($connection, $resource_details);
    $author = mysqli_real_escape_string($connection, $author);
    $datepub = mysqli_real_escape_string($connection, $datepub);

    $genres_idquery = "Select coalesce(max(resource_id), 0) + 1 as resource_id from resource";
    $g2 = mysqli_query($connection, $genres_idquery);
    $row_genresid = mysqli_fetch_assoc($g2);
    $genres_id = $row_genresid['resource_id'];
    $query_genresource= mysqli_query($connection, "INSERT INTO resource (resource_id, resource_name, resource_details, resource_author, date_published) VALUES ($genres_id, '$resource_name', '$resource_details', '$author', '$datepub')");
        if($query_genresource){
        echo "<script type='text/javascript'>alert('Resource Created Successfully')</script>";
        echo "<script>window.location='resources.php';</script>";
        echo "<script>close()</script>";
        }
 }
 if(isset($_POST["searchidnum"])){
    $user_id = $_POST["idnum"];
    $querycheckuser = "Select * from basic_user_info where id_number = '$user_id'";
    $qcu = mysqli_query($connection, $querycheckuser);
    if(mysqli_num_rows($qcu) < 1){
        echo "<script type='text/javascript'>alert('Unknown ID Number!')</script>";
        echo "<script>window.location='resources.php';</script>";
        echo "<script>close()</script>";
    }
    else{
        echo "<script>window.location='addResourceTransaction.php?idnum=$user_id';</script>";
        echo "<script>close()</script>";
    }
 }

 if(isset($_POST["borrowbook2"])){
    $user_id = $_POST["name"];
    $resource_id = $_POST["resourcedata"];

    $user_id = mysqli_real_escape_string($connection, $user_id);
    $resource_id = mysqli_real_escape_string($connection, $resource_id);

    $genres_idquery = "Select coalesce(max(resource_transaction_id), 0) + 1 as resource_transaction_id from resource_transactions";
    $g2 = mysqli_query($connection, $genres_idquery);
    $row_genresid = mysqli_fetch_assoc($g2);
    $genres_id = $row_genresid['resource_transaction_id'];
    $query_genrestransaction = mysqli_query($connection, "INSERT INTO resource_transactions (resource_transaction_id, resource_id, name, admin_id, rt_datetime) VALUES ($genres_id,$resource_id, '$user_id', $uid, CURRENT_TIMESTAMP)");
    if($query_genrestransaction){
        $genres_idquerylog = "Select coalesce(max(log_id), 0) + 1 as log_id from logs";
        $g3 = mysqli_query($connection, $genres_idquerylog);
        $row_genreslog = mysqli_fetch_assoc($g3);
        $genres_logid = $row_genreslog['log_id'];
        $query_res_logs = mysqli_query($connection, "INSERT INTO logs (log_id, log_type, log_time, resource_transaction_id) VALUES ($genres_logid,'resource_transaction', CURRENT_TIMESTAMP,  $genres_id)");
        if($query_res_logs){
        echo "<script type='text/javascript'>alert('Borrowed Resource Successfully')</script>";
        echo "<script>window.location='logs.php?tosort=all#tab3';</script>";
        echo "<script>close()</script>";
        }
    }   
 }
 $query_2 = "SELECT * FROM resource";
 $result_2 = mysqli_query($connection, $query_2);
?>  

<html>
  <head>
      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <title>SERDAC System</title>
      <link type="text/css" rel="stylesheet" href="css/stylesheet.css" />
      <link type="text/css" rel="stylesheet" href="css/materialize.css" media="screen,projection" />

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <link rel="icon" href="images/favicon.ico" type="image/ico" sizes="16x16">
      <div class="navbar-fixed">
            <nav>
                <div class="nav-wrapper blue row">
                <a href="admindashboard.php" class="brand-logo left">SERDAC</a>
                    <div class="container">
                            <ul id="nav-mobile" class="left hide-on-med-and-down">
                                <li><a href="logs.php?tosort=all">Logs</a></li>
                                <li class="active"><a href="resources.php">Resources</a></li>
                                <li><a href="services.php">Services</a></li>
                                <li><a href="accounts.php?tosearch=all">Accounts</a><li>
                            </ul>
                    </div>
                    <ul id="nav-mobile" class="right hide-on-med-and-down">
                        <a href="include/logout_admin.php">Logout</a>
                    </ul>   
                </div>
            </nav>
        </div>
  </head>
<body>

    <div class="container">
      <div class="row">        
        <div class="col s12 center-align">
              <div class="container">
                    <ul class="tabs">
                              <li class="tab col s4 grey lighten-4"><a href="#tab1">Use Resource</a></li>
                              <li class="tab col s4 grey lighten-4"><a href="#tab2">View Resources</a></li>
                    </ul>    
                    
                    <div id="tab1">
                        <h4 class="left-align"><u>Use Resource</u>
                            </h4> 
                     <form action="" method="POST">
                        <div class="row">
                            <div class="input-field col s12">
                                <input type="text" name="name" id="name" class="validate" required>
                                <label for="name">Name</label>
                            </div>    
                        </div>
                        <div class="row">
                                <div class ="input-field col s12">        
                                    <select name="resourcedata">
                                        <?php 
                                            while($row_res = mysqli_fetch_assoc($result_2)){
                                        ?>
                                        <option value="<?php echo $row_res['resource_id'] ?>" ><?php echo $row_res['resource_name'] ?></option><?php
                                            }
                                        ?>
                                    </select>
                                <label>Resources</label>
                                </div>
                        </div>
                        
                        <button class="btn-large blue" type="submit" name="borrowbook2">Use Resource</button>
                   
                    </form>
                    </div>
                    <div id="tab2">
                            <h4 class="left-align"><u>Resources</u>
                            <a class="btn-floating btn-medium blue modal-trigger right hide-on-med-and-down btn tooltipped" data-position="right" data-tooltip="Add Resource" href="#addResource"><i class="material-icons">add</i></a>
                            </h4>
                            <div id="welcomeDiv">
                            <table class="centered">
                                        <thead>
                                        <?php 
                                                $query_logs3  = "SELECT * FROM `resource_view`";// resource view
                                                $results_logs3 = mysqli_query($connection, $query_logs3);
                                                if(mysqli_num_rows($results_logs3) < 1){
                                                    ?> <h4>No Resources</h4><?php
                                                }
                                                else{
                                        ?>
                                            <tr>
                                                <th>Resource Name</th>
                                                <th>Resource Details</th>
                                                <th>Author</th>
                                                <th>Date Published</th>
                                                <th>Edit</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                                while($results3 = mysqli_fetch_assoc($results_logs3)){
                                            ?>
                                            <tr>
                                                <td><?php echo $results3['resource_name'] ?></td>
                                                <td><?php echo $results3['resource_details'] ?></td>
                                                <td><?php echo $results3['resource_author'] ?></td>
                                                <td><?php echo $results3['date_published'] ?></td>
                                            
                                                <td>
                                                    <a name="toedit" href="optionsResources.php?number=<?php echo $results3['resource_id'];?>">Edit
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php }} ?>
                                        </tbody>
                                    </table> 
                                </div>    
                    </div>
            </div>   
        </div>
      </div>
    </div> 

    <div id="addResource" class="modal">
              <div class="modal-content">
              <div class="center-align">
              <h4>Resources</h4>
                <div class="container">
                <form action="" method="POST">
                <div class="row">
                        <div class="input-field col s12">
                            <input type="text" name="resource_name" id="resource_name" class="validate" >
                            <label for="resource_name">Resource Name</label>
                        </div>       
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" name="resource_details" id="resource_details" class="validate" >
                            <label for="resource_details">Resource Details</label>
                        </div>       
                    </div>
                    <div class="row">
                            <div class="input-field col s12">
                                <input type="text" name="author" id="author" class="validate">
                                <label for="author">Resource Author</label>
                            </div>    
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input type="date" name="datepub" id="datepub" class="validate">
                                <label for="datepub">Date Published</label>
                            </div>    
                        </div>
                    <div class="row center-align">
                            <button class="btn-large blue" type="submit" name="add_Go">Add Resource</button>
                    </div>
                </div>
                </form>
            </div>
          </div>
        </div>
    </div>
</body>
  <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/materialize.min.js">
    </script>
   <script>$(document).ready(function(){
    $('select').formSelect();
  });

    var elem1 = document.querySelector('#addResource');
    var instance1 = M.Modal.init(elem1);

    var elem2 = document.querySelector('#viewResource');
    var instance2 = M.Modal.init(elem2);

    $(document).ready(function(){
    $('.tooltipped').tooltip();
  });
    </script>

      <script>
    $(document).ready(function(){
         $('.tabs').tabs();
    });</script>  
</html>