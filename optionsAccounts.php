<?php
 include("include/session_admin.php");
 require_once("include/conn.php");
 $uid = $_SESSION['admin_uid'];
 $var_value = $_GET['number'];

 $query_2 = "SELECT * FROM basic_user_info where user_id = $var_value";
 $result_2 = mysqli_query($connection, $query_2);
 $countres = mysqli_num_rows($result_2);
 $showres = mysqli_fetch_assoc($result_2);

if($countres < 1){
    header("Location: accounts.php");
}

 if(isset($_POST["edit_Go"])){
    $firstname = $_POST["firstname"];
    $lastname = $_POST["lastname"];
    $school = $_POST["school"];
    $id_number = $_POST["id_number"];
    $course = $_POST["course"];
    $contact_number = $_POST["contact_number"];
    $designation = $_POST["designation"];

    $firstname = mysqli_real_escape_string($connection, $firstname);
    $lastname = mysqli_real_escape_string($connection, $lastname);
    $school = mysqli_real_escape_string($connection, $school);
    $id_number = mysqli_real_escape_string($connection, $id_number);
    $course = mysqli_real_escape_string($connection, $course);
    $contact_number = mysqli_real_escape_string($connection, $contact_number);
    $designation = mysqli_real_escape_string($connection, $designation);

    $query_11 = "SELECT * FROM basic_user_info WHERE id_number='$id_number' and user_id <> $var_value";
	$result1 = mysqli_query($connection, $query_11);
    if(mysqli_num_rows($result1) >= 1){
        echo "<script type='text/javascript'>alert('ID Number Already Taken!!')</script>";
        echo "<script>window.location='optionsAccounts.php?number=$var_value';</script>";
        echo "<script>close()</script>";
    }
    else{
    $query_updateacc= mysqli_query($connection, "UPDATE basic_user_info SET firstname = '$firstname',
     lastname = '$lastname',  id_number = '$id_number', course = '$course', contact_number = '$contact_number',
     designation = '$designation' where user_id = $var_value");
        if($query_updateacc){
        echo "<script type='text/javascript'>alert('Account Details Updated Successfully')</script>";
        echo "<script>window.location='accounts.php?tosearch=all';</script>";
        echo "<script>close()</script>";
        }
        else{
        echo "<script type='text/javascript'>alert('Update Failed!')</script>";
        echo "<script>window.location='optionsAccounts.php?number=$var_value';</script>";
        echo "<script>close()</script>";
        }
    }
 }
?>  

<html>
  <head>
      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <title>SERDAC System</title>
      <link type="text/css" rel="stylesheet" href="css/stylesheet.css" />
      <link type="text/css" rel="stylesheet" href="css/materialize.css" media="screen,projection" />

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <link rel="icon" href="images/favicon.ico" type="image/ico" sizes="16x16">
      <div class="navbar-fixed">
            <nav>
                <div class="nav-wrapper blue row">
                <a href="admindashboard.php" class="brand-logo left">SERDAC</a>
                    <div class="container">
                            <ul id="nav-mobile" class="left hide-on-med-and-down">
                            </ul>
                    </div>
                    <ul id="nav-mobile" class="right hide-on-med-and-down">
                        <a href="include/logout_admin.php">Logout</a>
                    </ul>   
                </div>
            </nav>
        </div>
  </head>
<body>

    <div class="container">
      <div class="row">       
        <div class="container">
              <div class="center-align">  
              <ul class="collection right hide-on-med-and-down">
                <li><a href="#editAccount" class="btn-floating btn-large blue modal-trigger tooltipped" data-position="right" data-tooltip="Edit"><i class="material-icons">edit</i></a>
                </li>
                <li><a href="addResourceTransaction.php?idnum=<?php echo $showres['id_number'];?>" class="btn-floating btn-large blue modal-trigger tooltipped" data-position="right" data-tooltip="Use Resource"><i class="material-icons">note_add</i></a>
                </li>
                <li><a href="addServiceTransaction.php?idnum=<?php echo $showres['id_number'];?>" class="btn-floating btn-large blue modal-trigger tooltipped" data-position="right" data-tooltip="Use Services"><i class="material-icons">add_to_queue</i></a>
                </li>
                   
                </ul>
              
              <h4>
              
              <a href="accounts.php?tosearch=all" class="btn-floating btn-medium blue left hide-on-med-and-down"><i class="material-icons">arrow_back</i></a>
              <u>Account Information</u></h4>
                   <div class="container"> 
                   <div class="left-align">  
                   <p>
                    <h6><b>Name:</b> <?php echo $showres['firstname']." ".$showres['lastname']; ?></h6>        
                    </p>
                    <p>
                    <h6><b>Id Number:</b> <?php echo $showres['id_number']?></h6>
                    </p>                       
                    <p>
                    <h6><b>Course:</b> <?php echo $showres['course']?></h6>
                    </p>                       
                    <p>
                    <h6><b>School:</b> <?php echo $showres['school']?></h6>
                    </p>                       
                    <p>
                    <h6><b>Contact Number:</b> <?php echo $showres['contact_number']?></h6>
                    </p>                       
                    <p>
                    <h6><b>Designation:</b> <?php echo $showres['designation']?></h6>
                    </p>                       
                    <p >
                    <h6><b>Date of Registration:</b> <?php
                     $thedate = strtotime($showres['date_joined']);
                     echo date('F j, Y', $thedate);
                   ?></h6>
                    </p>                       
                    
                    </div>
                </div>
              </div>
        </div> 
      </div>
    </div>  
    <div id="editAccount" class="modal">
          <div class="container">
              <div class="center-align">
             
              <form action="" method="POST">
                    <h4>
                Edit Account Information</h4>
                    <div class="row">
                        <div class="input-field col s6">
                            <input id="first_name" name="firstname" type="text" class="validate" value="<?php echo $showres["firstname"] ?>" required>
                            <label for="first_name">First Name</label>
                        </div>
                        <div class="input-field col s6">
                            <input id="last_name" name="lastname" type="text" class="validate" value="<?php echo $showres["lastname"] ?>" required>
                            <label for="last_name">Last Name</label>
                        </div>        
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" name="id_number" id="id_number" class="validate" value="<?php echo $showres["id_number"] ?>" required>
                            <label for="id_number">ID Number</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" name="school" id="school" class="validate" value="<?php echo $showres["school"] ?>">
                            <label for="school">School</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" name="course" id="course" class="validate" value="<?php echo $showres["course"] ?>">
                            <label for="course">Course</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" name="contact_number" id="contact_number" class="validate" value="<?php echo $showres["contact_number"] ?>">
                            <label for="contact_number">Contact Number</label>
                        </div>       
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" name="designation" id="designation" class="validate" value="<?php echo $showres["designation"] ?>">
                            <label for="designation">Designation</label>
                        </div>       
                    </div>
                      <div class="row">
                     
                        <button class="btn-large blue" type="submit" name="edit_Go">Edit</button>
                       </div>
                    </div>
                   
                </form>
              </div>
            </div>
    </div>
</body>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/materialize.min.js">
    </script>
    <script>
     var elem2 = document.querySelector('#editAccount');
    var instance2 = M.Modal.init(elem2);
    </script>
    <script>
    $(document).ready(function(){
    $('.tooltipped').tooltip();
  });
    </script>
  
</html>