<?php
 include("include/session_admin.php");
 require_once("include/conn.php");
 $uid = $_SESSION['admin_uid'];
 $var_value = $_GET['number'];

 $query_2 = "SELECT * FROM resource where resource_id = $var_value";
 $result_2 = mysqli_query($connection, $query_2);
 $countres = mysqli_num_rows($result_2);
 $showres = mysqli_fetch_assoc($result_2);

if($countres < 1){
    header("Location: resources.php#tab2");
}

 if(isset($_POST["edit_Go"])){
    $resource_name = $_POST["rname"];
    $resource_details = $_POST["rdetails"];
    $author = $_POST["author"];
    $datepub = $_POST["datepub"];

    $resource_name = mysqli_real_escape_string($connection, $resource_name);
    $resource_details = mysqli_real_escape_string($connection, $resource_details);

    $query_updateresource= mysqli_query($connection, "UPDATE resource SET resource_name = '$resource_name', resource_details = '$resource_details', resource_author = '$author', date_published = '$datepub' where resource_id = $var_value");
        if($query_updateresource){
        echo "<script type='text/javascript'>alert('Resource Updated Successfully')</script>";
        echo "<script>window.location='resources.php#tab2';</script>";
        echo "<script>close()</script>";
        }
        else{
          echo "<script type='text/javascript'>alert('Resource Update Failed!')</script>";
          echo "<script>window.location='optionsServices.php?number=$var_value';</script>";
          echo "<script>close()</script>";
        }
 }
?>  

<html>
  <head>
      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <title>SERDAC System</title>
      <link type="text/css" rel="stylesheet" href="css/stylesheet.css" />
      <link type="text/css" rel="stylesheet" href="css/materialize.css" media="screen,projection" />

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <link rel="icon" href="images/favicon.ico" type="image/ico" sizes="16x16">
      <div class="navbar-fixed">
            <nav>
                <div class="nav-wrapper blue row">
                <a href="admindashboard.php" class="brand-logo left">SERDAC</a>
                    <div class="container">
                            <ul id="nav-mobile" class="left hide-on-med-and-down">
                            </ul>
                    </div>
                    <ul id="nav-mobile" class="right hide-on-med-and-down">
                        <a href="include/logout_admin.php">Logout</a>
                    </ul>   
                </div>
            </nav>
        </div>
  </head>
<body>

    <div class="container">
      <div class="row">       
        <div class="col s12">
              <div class="container">
              <div class="center-align">
              <h4>
              
              <a href="resources.php#tab2" class="btn-floating btn-medium blue left hide-on-med-and-down"><i class="material-icons">arrow_back</i></a>
              Edit Resource</h4>
              
              <form action="" method="POST">
                  <div class="row">
                    <div class="col s12">
                      <div class="row">
                        <div class="input-field offset-s1 col s10">
                          <input id="rname" name="rname" type="text" class="validate" value="<?php echo $showres["resource_name"] ?>">
                          <label for="rname">Resource Name</label>
                        </div>
                      </div>
                      <div class="row">
                        <div class="input-field offset-s1 col s10">
                          <input id="rdetails" name="rdetails" type="text" class="validate" value="<?php echo $showres["resource_details"] ?>">
                          <label for="rdetails">Resource Details</label>
                        </div>
                      </div>
                      <div class="row">
                            <div class="input-field offset-s1 col s10">
                                <input type="text" name="author" id="author" class="validate" value="<?php echo $showres["resource_author"] ?>">
                                <label for="author">Resource Author</label>
                            </div>    
                        </div>
                        <div class="row">
                            <div class="input-field offset-s1 col s10">
                                <input type="date" name="datepub" id="datepub" class="validate" value="<?php echo $showres["date_published"] ?>">
                                <label for="datepub">Date Published</label>
                            </div>    
                        </div>
                      <div class="row">
                     
                        <button class="btn-large blue" type="submit" name="edit_Go">Edit</button>
                       </div>
                    </div>
                  </div>
                  </div> 
                </form>
              </div>
            </div>     
      </div>
    </div> 
</body>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/materialize.min.js">
    </script>
    <script>

    $(document).ready(function(){
    $('.tooltipped').tooltip();
  });

    </script>
  
</html>