<?php
 include("include/session_admin.php");
 include("include/functions.php");
 require_once("include/conn.php");
 $uid = $_SESSION['admin_uid'];
 $tosort = $_GET['tosort'];
 $finaltoDate = '';

$tosortList = explodeList($tosort);
for($a = 0;$a < count($tosortList); $a++){
    $tosortList[$a];
 }
 if(count($tosortList)>=3){/*
    if(validateDate($tosortList[0])){
        echo "<script type='text/javascript'>alert('Error!')</script>";
        echo "<script>window.location='logs.php?tosort=all';</script>";
        echo "<script>close()</script>";
    }
    if(validateDate($tosortList[1])){
        echo "<script type='text/javascript'>alert('Error!')</script>";
        echo "<script>window.location='logs.php?tosort=all';</script>";
        echo "<script>close()</script>";
    }
    if(($tosortList[2]<>'ascending')||($tosortList[2]<>'descending')){
        echo "<script type='text/javascript'>alert('Error!')</script>";
        echo "<script>window.location='logs.php?tosort=all';</script>";
        echo "<script>close()</script>";
    }*/
    if(($tosortList[0] >= $tosortList[1])){
    echo "<script type='text/javascript'>alert('Incorrect Date Condition!!!')</script>";
    echo "<script>window.location='logs.php?tosort=all';</script>";
    echo "<script>close()</script>";}
 }
 if(count($tosortList)>=3){
 $finaltoDate = date('Y-m-d', strtotime($tosortList[1] . ' +1 day'));}
 if(isset($_POST["deletest"])){
    $user_id = $_POST["todel"];
    $querycheckuser = "Select * from logs WHERE service_transaction_id = $user_id";
    $qcu = mysqli_query($connection, $querycheckuser);
    $row_qcu = mysqli_fetch_assoc($qcu);
    $stoDel = $row_qcu['log_id'];
    $querydelst = "DELETE from logs WHERE log_id = $stoDel";
    $qst = mysqli_query($connection, $querydelst);
    if($qst){
        $querydelst2 = "DELETE from service_transactions WHERE service_transaction_id = $user_id";
        $qst2 = mysqli_query($connection, $querydelst2);
        if($qst2){
        echo "<script type='text/javascript'>alert('Delete Succeeded!')</script>";
        echo "<script>window.location='logs.php?tosort=all#tab4';</script>";
        echo "<script>close()</script>";}
    }
 }
 if(isset($_POST["sort_Go"])){

    $fromdate = $_POST['fromdate'];
    $todate = $_POST['todate'];
    $datetoSort = $_POST['datesort'];
   
     if($fromdate >= $todate){
        echo "<script type='text/javascript'>alert('Incorrect Date Condition!!!')</script>";
        echo "<script>window.location='logs.php?tosort=all';</script>";
        echo "<script>close()</script>";
     }
     else{
        $fromdate2 = date('Y-m-d', strtotime($fromdate));
        $todate2 = date('Y-m-d', strtotime($todate));
        echo "<script>window.location='logs.php?tosort=$fromdate2,$todate2,$datetoSort';</script>";
        echo "<script>close()</script>";
    }
 }
?>  

<html>
  <head>
      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <title>SERDAC System</title>
      <link type="text/css" rel="stylesheet" href="css/stylesheet.css" />
      <link type="text/css" rel="stylesheet" href="css/materialize.css" media="screen,projection" />

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <link rel="icon" href="images/favicon.ico" type="image/ico" sizes="16x16">
      <div class="navbar-fixed">
            <nav>
                <div class="nav-wrapper blue row">
                <a href="admindashboard.php" class="brand-logo left">SERDAC</a>
                    <div class="container">
                            <ul id="nav-mobile" class="left hide-on-med-and-down">
                                <li class="active"><a href="logs.php?tosort=all">Logs</a></li>
                                <li><a href="resources.php">Resources</a></li>
                                <li><a href="services.php">Services</a></li>
                                <li><a href="accounts.php?tosearch=all">Accounts</a><li>
                            </ul>
                    </div>
                    <ul id="nav-mobile" class="right hide-on-med-and-down">
                        <a href="include/logout_admin.php">Logout</a>
                    </ul>   
                </div>
            </nav>
        </div>

  </head>
<body>

    <div class="container">
      <div class="row">
        <!--login and registration buttons-->
        <div class="col s12 center-align">
        <h4> 
         <a href="#tosort" class="btn-floating btn-medium blue left hide-on-med-and-down modal-trigger tooltipped" data-position="right" data-tooltip="Sort Options"><i class="large material-icons">sort</i></a>
                    Logs</h4>
                <ul class="tabs">
                    <li class="tab col s4 grey lighten-4"><a href="#tab1">Login & Logout</a></li>
                    <li class="tab col s4 grey lighten-4"><a href="#tab3">Resources</a></li>
                    <li class="tab col s4 grey lighten-4"><a href="#tab4">Services</a></li>
                </ul>
                
              <div id="tab1">
                    <div id="tabb1">
                     <table class="left" id="log1">
                         <thead>
                         <?php 
                                if(count($tosortList)==3){
                                    if($tosortList[2]=='ascending'){
                                        $query_logs1  = " SELECT CONCAT(b.firstname, ' ', b.lastname) AS name, b.designation, b.school, l.log_type, l.log_time, l.log_id FROM basic_user_info b, logs l WHERE (l.log_time >= '$tosortList[0]' and l.log_time <= '$finaltoDate') and (l.log_type='user_login' or l.log_type='user_logout') and (b.user_id = l.user_id) order by l.log_time ASC";       
                                        $results_logs1 = mysqli_query($connection, $query_logs1);
                                    }
                                    else if($tosortList[2]=='descending'){
                                        $query_logs1  = " SELECT CONCAT(b.firstname, ' ', b.lastname) AS name, b.designation, b.school, l.log_type, l.log_time, l.log_id FROM basic_user_info b, logs l WHERE (l.log_time >= '$tosortList[0]' and l.log_time <= '$finaltoDate') and (l.log_type='user_login' or l.log_type='user_logout') and (b.user_id = l.user_id) order by l.log_time desc";       
                                        $results_logs1 = mysqli_query($connection, $query_logs1);
                                    }
                                 }   
                                else{
                                $query_logs1  = "SELECT CONCAT(b.firstname, ' ', b.lastname) AS name, b.designation, b.school, l.log_type, l.log_time, l.log_id FROM basic_user_info b, logs l WHERE l.log_type='user_login' and b.user_id = l.user_id or l.log_type='user_logout' and b.user_id = l.user_id order by log_id DESC";       
                                $results_logs1 = mysqli_query($connection, $query_logs1);
                                }
                                if(mysqli_num_rows($results_logs1) < 1){
                                    ?> <h4>No Log Records</h4><?php
                                }
                                else{
                        ?>
                             <tr>
                                <th>Name</th>
                                <th>Designation</th>
                                <th>School</th>
                                <th>Entry</th>
                               
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                 while($results1 = mysqli_fetch_assoc($results_logs1)){
                                 ?>
                            <tr>
                                <td><?php echo $results1['name'];?></td>
                                <td><?php echo $results1['designation']; ?></td>
                                <td><?php echo $results1['school']; ?></td>
                                <td><?php 
                                if($results1['log_type'] == 'user_login'){
                                    echo "LOG-IN";
                                    $thedate = strtotime($results1['log_time']);
                                    echo date(' F j, Y g:i a', $thedate);
                                }
                                else if($results1['log_type'] == 'user_logout'){
                                    echo "LOG-OUT";
                                    $thedate = strtotime($results1['log_time']);
                                    echo date(' F j, Y g:i a', $thedate);
                                }  
                                ?></td>
                            </tr>
                            <?php }} ?>
                        </tbody>
                    </table>
                    </div>
                    <a class="btn right-align" onclick="printDiv('tabb1')">print</a>
                </div>        
                <div id="tab3">
                    <div id="tabb3">
                     <table class="centered">
                         <thead>
                         <?php 
                                 /*$query_logs3  = "SELECT CONCAT(b.firstname, ' ', b.lastname) AS name, r.resource_name, rt.rt_datetime , rt.guest_id, rt.user_id  FROM resource_transactions rt, basic_user_info b, resource r where rt.user_id = b.user_id and r.resource_id = rt.resource_id order by rt.rt_datetime DESC";
                                */
                                if(count($tosortList)==3){
                                    if($tosortList[2]=='ascending'){
                                        $query_logs3  = "SELECT r.resource_name, rt.resource_transaction_id, rt.user_id, rt.guest_id, rt.resource_id, rt.rt_datetime, rt.name FROM resource_transactions rt, resource r where (rt.rt_datetime >= '$tosortList[0]' and rt.rt_datetime <= '$finaltoDate') and 
                                        r.resource_id = rt.resource_id order by rt.rt_datetime asc";
                                    }
                                    else if($tosortList[2]=='descending'){
                                        $query_logs3  = "SELECT r.resource_name, rt.resource_transaction_id, rt.user_id, rt.guest_id, rt.resource_id, rt.rt_datetime, rt.name FROM resource_transactions rt, resource r where (rt.rt_datetime >= '$tosortList[0]' and rt.rt_datetime <= '$finaltoDate') and 
                                        r.resource_id = rt.resource_id order by rt.rt_datetime desc";
                                    }
                                 }   
                                else{
                                    $query_logs3  = "SELECT r.resource_name, rt.resource_transaction_id, rt.user_id, rt.guest_id, rt.resource_id, rt.rt_datetime, rt.name FROM resource_transactions rt, resource r where r.resource_id = rt.resource_id order by rt.rt_datetime desc";
                                }
                                $results_logs3 = mysqli_query($connection, $query_logs3);
                                 if(mysqli_num_rows($results_logs3) < 1){
                                    ?> <h4>No Resource Logs</h4><?php
                                }
                                else{
                        ?>
                             <tr>
                                <th>Name</th>
                                <th>Resource Borrowed</th>
                                <th>Date</th>
                                <th>Time</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                 while($results3 = mysqli_fetch_assoc($results_logs3)){
                            ?>
                            <tr>
                                <td>
                                <?php 
                                if($results3['user_id']<>''){
                                    $qw = $results3['resource_transaction_id'];
                                    $querytempres = "SELECT CONCAT(b.firstname, ' ', b.lastname) AS name, r.resource_name FROM resource r, resource_transactions rt, basic_user_info b where rt.resource_id = r.resource_id and rt.user_id = b.user_id and rt.resource_transaction_id = $qw";
                                    $qtp = mysqli_query($connection, $querytempres);
                                    $resultqtp = mysqli_fetch_assoc($qtp);
                                    echo $resultqtp['name'];
                                } 
                                else{
                                    echo $results3['name'];
                                    } 
                                ?>
                                </td>
                                <td><?php echo $results3['resource_name'] ?></td>
                               
                                <td><?php 
                                 $thedate = strtotime($results3['rt_datetime']);
                                echo date('F j, Y', $thedate);?></td>
                                
                                <td><?php
                                echo date('g:i a', $thedate);?></td>
                            </tr>
                            <?php }} ?>
                        </tbody>
                    </table>
                    </div>
                    <a class="btn right-align" onclick="printDiv('tabb3')">print</a>
                </div>
                <div id="tab4">
                    <table class="centered">
                         <thead>
                         <?php
                          if(count($tosortList)==3){
                            if($tosortList[2]=='ascending'){
                                $query_logs4  = "SELECT st.user_id, st.name, s.service_name, st.total_fee, st.st_datetime, st.service_transaction_id FROM service_transactions st, services s where (st.st_datetime >= '$tosortList[0]' and st.st_datetime <= '$finaltoDate') and s.service_id = st.service_id order by st.st_datetime ASC";
                           
                            }
                            else if($tosortList[2]=='descending'){
                                 $query_logs4  = "SELECT st.user_id, st.name, s.service_name, st.total_fee, st.st_datetime, st.service_transaction_id FROM service_transactions st, services s where (st.st_datetime >= '$tosortList[0]' and st.st_datetime <= '$finaltoDate') and s.service_id = st.service_id order by st.st_datetime DESC";
                            }
                         }   
                        else{
                            $query_logs4  = "SELECT st.user_id, st.name, s.service_name, st.total_fee, st.st_datetime, st.service_transaction_id FROM service_transactions st, services s where s.service_id = st.service_id order by st.st_datetime DESC";
                           }
                           $results_logs4 = mysqli_query($connection, $query_logs4);
                           
                          if(mysqli_num_rows($results_logs4) < 1){
                              ?> <h4>No Service Logs</h4><?php
                          }
                          else{
                         ?>
                             <tr>
                                <th>Name</th>
                                <th>Service Purchased</th>
                                <th>Date</th>
                                <th>Time</th>
                               <th>Total Fee</th>
                                <th>View</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                 while($results4 = mysqli_fetch_assoc($results_logs4)){
                                 ?>
                            <tr>
                                <td><?php 
                                if($results4['user_id']<>''){
                                    $qw = $results4['service_transaction_id'];
                                    $querytempres = "SELECT CONCAT(b.firstname, ' ', b.lastname) AS name FROM services s, service_transactions st, basic_user_info b where st.service_id = s.service_id and st.user_id = b.user_id and st.service_transaction_id = $qw";
                                    $qtp = mysqli_query($connection, $querytempres);
                                    $resultqtp = mysqli_fetch_assoc($qtp);
                                    echo $resultqtp['name'];
                                } 
                                else{
                                echo $results4['name'];} ?></td>
                                <td><?php echo $results4['service_name'] ?></td>
                                <td><?php 
                                 $thedate = strtotime($results4['st_datetime']);
                                echo date('F j, Y', $thedate);?></td>
                                
                                <td><?php
                                echo date('g:i a', $thedate);?></td>
                                 <td><?php echo $results4['total_fee'] ?></td>
                                <td>
                                <a href="viewServiceTransaction.php?number=<?php echo $results4['service_transaction_id'];?>">View
                                    </a>
                                </td>
                            </tr>
                            <?php }} ?>
                        </tbody>
                    </table>
                    
                    <a class="btn right-align" onclick="printDiv('tabb4')">print</a>
                </div> <?php if(count($tosortList)>=3){?>
                <p class="left-align"><b>Sorted from: <?php echo date('F j, Y', strtotime($tosortList[0]));?> - 
                <?php echo date('F j, Y', strtotime($tosortList[1]));}?></b>
                </p>
        </div>
      </div>
    </div> 
    
    <div id="tosort" class="modal">
              <div class="modal-content">
              <div class="center-align">
              <h4>Sort Options</h4>
                <div class="container">
                <form action="" method="POST">
                    <div class="row">
                            <div class="input-field col s6">
                                <input type="date" name="fromdate" id="fromdate" value="<?php if(count($tosortList)>=3){echo $tosortList[0];} ?>">
                                <label for="fromdate">From</label>
                            </div>         
                            <div class="input-field col s6">
                                <input type="date" name="todate" id="todate" value="<?php if(count($tosortList)>=3){echo $tosortList[1];} ?>">
                                <label for="todate">To</label>
                            </div> 
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <select name="datesort">
                                <option value="ascending">Ascending</option>
                                <option value="descending" selected>Descending</option>
                            </select>
                            <label>Date Sort</label>
                        </div>
                    </div>
                    <div class="row center-align">
                            <button class="btn-large blue" type="submit" name="sort_Go">Sort</button>
                            <a class="btn-large blue" href="logs.php?tosort=all">View All<a>
                    </div>
                    </form>
                </div>
                
          </div>
         
        </div>
    </div>  
                      
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script> 
    <script>
    $(document).ready(function(){
         $('.tabs').tabs();
    });
    
    </script>  
    <script>
    var elem2 = document.querySelector('#tosort');
    var instance2 = M.Modal.init(elem2);

    </script>
    <script>
     function deleteFunction(num){
         if (confirm("Delete this log?")) {
            window.location='logs.php';
        } else {

        }
    }
    </script>
    <script>
    $(document).ready(function(){
    $('.tooltipped').tooltip();
  });
    </script>
    <script>$(document).ready(function(){
    $('select').formSelect();
  });
    </script>
                        <script>
                            function printDiv(divName) {
                                var printContents = document.getElementById(divName).innerHTML;
                                var originalContents = document.body.innerHTML;

                                document.body.innerHTML = printContents;
                                
                                window.print();
                                document.body.innerHTML = originalContents;
                                location.reload();
                            }
                        </script>
</body>
</html>