<?php
  include("include/session_admin.php");
  require_once("include/conn.php");
  $uid = $_SESSION['admin_uid'];
  $idnum = $_GET['number'];

  $idnumchecker = "SELECT b.user_id from service_transactions s, basic_user_info b where b.user_id=s.user_id and s.service_transaction_id = $idnum";
  $qcu1 = mysqli_query($connection, $idnumchecker);
  $countidnumc = mysqli_num_rows($qcu1);

  if($countidnumc>0){
  $querycheckuser = "SELECT CONCAT(b.firstname, ' ', b.lastname) AS name, b.firstname, b.id_number, s.service_name, s.fixed_rate, st.total_fee, st.extra_fee, st.remarks FROM service_transactions st, basic_user_info b, services s WHERE b.user_id = st.user_id
   and st.service_id = s.service_id and st.service_transaction_id = $idnum";}
  else{
      $querycheckuser = "SELECT st.name, s.service_name, s.fixed_rate, st.total_fee, st.extra_fee, st.remarks FROM service_transactions st, services s WHERE st.service_id = s.service_id and st.service_transaction_id = $idnum";
    }
  $qcu = mysqli_query($connection, $querycheckuser);
  $row_qcu = mysqli_fetch_assoc($qcu);
  $extrafee = $row_qcu["extra_fee"];
 
  $query_2 = "SELECT * FROM service";
  $result_2 = mysqli_query($connection, $query_2);
 
 ?>
<html>
  <head>
      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <title>SERDAC System</title>
      <link type="text/css" rel="stylesheet" href="css/stylesheet.css" />
      <link type="text/css" rel="stylesheet" href="css/materialize.css" media="screen,projection" />

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <link rel="icon" href="images/favicon.ico" type="image/ico" sizes="16x16">
      <div class="navbar-fixed">
            <nav>
                <div class="nav-wrapper blue row">
                <a href="admindashboard.php" class="brand-logo left">SERDAC</a>
                    <ul id="nav-mobile" class="right hide-on-med-and-down">
                        <a href="include/logout_admin.php">Logout</a>
                    </ul>   
                </div>
            </nav>
        </div>
  </head>
<body>

    <div class="container">
      <div class="row">        
        <div class="col s12 center-align">
              <div class="container">
                        <h4 class="left-align"><u><?php
                        if($countidnumc>0){
                        echo $row_qcu["firstname"];}
                        else{
                            echo $row_qcu["name"];
                        }
                        ?>'s Transaction</u>
                            </h4> 
                                <input disabled value="<?php echo "Name: ".$row_qcu["name"]?>">
                                <input disabled value="<?php echo "Service Name: ".$row_qcu["service_name"]?>" id="disabled">
                                <input disabled value="<?php echo "Fee: ".$row_qcu["total_fee"]?>" id="disabled"> 
                                <input disabled value="<?php echo "Additional Fee: ".$extrafee?>" id="disabled">
                                <input disabled value="<?php echo "Total: ".($row_qcu["total_fee"]+$extrafee);?>" id="disabled"> 
                                <input disabled value="<?php echo "Remarks: ".$row_qcu["remarks"]?>" id="disabled"> 
                  
                    <a href="logs.php?tosort=all#tab4"><button class="btn-large blue">Back to Service Logs</button></a>
            </div>   
        </div>
      </div>
    </div> 

    
</body>
  <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/materialize.min.js">
    </script>
   <script>$(document).ready(function(){
    $('select').formSelect();
  });
    </script>
</html>