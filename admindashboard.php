<?php
 include("include/session_admin.php");
 require_once("include/conn.php");
 $uid = $_SESSION['admin_uid'];
?>  

<html>
  <head>
      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <title>SERDAC System</title>
      <link type="text/css" rel="stylesheet" href="css/stylesheet.css" />
      <link type="text/css" rel="stylesheet" href="css/materialize.css" media="screen,projection" />

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <link rel="icon" href="images/favicon.ico" type="image/ico" sizes="16x16">
      <div class="navbar-fixed">
            <nav>
                <div class="nav-wrapper blue row">
                    <a href="admindashboard.php" class="brand-logo">
                      SERDAC
                    </a>

                     <div class="nav-wrapper col s4 offset-s6 hide-on-med-and-down">
                    </div> 


                    <ul id="nav-mobile" class="right hide-on-med-and-down">
                        <a href="include/logout_admin.php">Logout</a>
                    </ul>
                </div>
            </nav>
        </div>

  </head>
<body>

    <div class="container">
      <div class="row center-align">
        <!--login and registration buttons-->
       
        <h4>SERDAC System Admin View</h4>
              <a href="logs.php?tosort=all"> <button class="btn-large blue">View Logs</button></a>
              <a href="resources.php"><button class="btn-large blue">Resources</button></a>
              <a href="services.php"><button class="btn-large blue">Services</button></a>
              <a href="accounts.php?tosearch=all"> <button class="btn-large blue">Accounts</button></a>
      </div>
    </div> 
<p>
</body>
</html>