<?php
  include("include/session_admin.php");
  require_once("include/conn.php");
  $uid = $_SESSION['admin_uid'];
  $idnum = $_GET['idnum'];

  $querycheckuser = "Select * from basic_user_info where id_number = '$idnum'";
  $qcu = mysqli_query($connection, $querycheckuser);
  $row_qcu = mysqli_fetch_assoc($qcu);
  if(mysqli_num_rows($qcu)<1){
    echo "<script>window.location='logs.php?tosort=all#tab3';</script>";
    echo "<script>close()</script>";
  }
  $query_2 = "SELECT * FROM resource";
  $result_2 = mysqli_query($connection, $query_2);
 if(isset($_POST["borrowbook2"])){
    $user_id = $_POST["names"];
    $resource_id = $_POST["resourcedata"];

    $user_id = mysqli_real_escape_string($connection, $user_id);
    $resource_id = mysqli_real_escape_string($connection, $resource_id);

    $genres_idquery = "Select coalesce(max(resource_transaction_id), 0) + 1 as resource_transaction_id from resource_transactions";
    $g2 = mysqli_query($connection, $genres_idquery);
    $row_genresid = mysqli_fetch_assoc($g2);
    $genres_id = $row_genresid['resource_transaction_id'];
    $query_genrestransaction = mysqli_query($connection, "INSERT INTO resource_transactions (resource_transaction_id, resource_id, user_id, admin_id, rt_datetime) VALUES ($genres_id,$resource_id, $user_id, $uid, CURRENT_TIMESTAMP)");
    if($query_genrestransaction){
        $genres_idquerylog = "Select coalesce(max(log_id), 0) + 1 as log_id from logs";
        $g3 = mysqli_query($connection, $genres_idquerylog);
        $row_genreslog = mysqli_fetch_assoc($g3);
        $genres_logid = $row_genreslog['log_id'];
        $query_res_logs = mysqli_query($connection, "INSERT INTO logs (log_id, log_type, log_time, resource_transaction_id) VALUES ($genres_logid,'resource_transaction', CURRENT_TIMESTAMP,  $genres_id)");
        if($query_res_logs){
        echo "<script type='text/javascript'>alert('Borrowed Resource Successfully')</script>";
        echo "<script>window.location='logs.php?tosort=all#tab3';</script>";
        echo "<script>close()</script>";
        }
    }   
 }
 ?>
<html>
  <head>
      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <title>SERDAC System</title>
      <link type="text/css" rel="stylesheet" href="css/stylesheet.css" />
      <link type="text/css" rel="stylesheet" href="css/materialize.css" media="screen,projection" />

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <link rel="icon" href="images/favicon.ico" type="image/ico" sizes="16x16">
      <div class="navbar-fixed">
            <nav>
                <div class="nav-wrapper blue row">
                <a href="admindashboard.php" class="brand-logo left">SERDAC</a>
                    <ul id="nav-mobile" class="right hide-on-med-and-down">
                        <a href="include/logout_admin.php">Logout</a>
                    </ul>   
                </div>
            </nav>
        </div>
  </head>
<body>

    <div class="container">
      <div class="row">        
        <div class="col s12 center-align">
              <div class="container">
                        <h4 class="left-align"><u>Use Resource</u>
                            </h4> 
                        <form action="" method="POST">    
                            <div class="row">
                                <div class="input-field col s12">
                                <input disabled value="<?php echo $row_qcu["firstname"]." ".$row_qcu["lastname"]." (".$row_qcu["id_number"].")"?>" id="disabled" type="text" class="validate">
                                <label for="disabled">Name</label>
                                </div>
                            </div>
                            <input type="hidden" name="names" value="<?php echo $row_qcu["user_id"]?>"/>
                            <div class="row">
                                <div class ="input-field col s12">        
                                    <select name="resourcedata">
                                        <?php 
                                            while($row_res = mysqli_fetch_assoc($result_2)){
                                        ?>
                                        <option value="<?php echo $row_res['resource_id'] ?>" ><?php echo $row_res['resource_name'] ?></option><?php
                                            }
                                        ?>
                                    </select>
                                <label>Resources</label>
                                </div>
                            </div>
                      
                    <button class="btn-large blue" type="submit" name="borrowbook2">Use Resource</button>
                    </form>
                    </div>
            </div>   
        </div>
      </div>
    </div> 

    
</body>
  <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/materialize.min.js">
    </script>
   <script>$(document).ready(function(){
    $('select').formSelect();
  });
    </script>
</html>