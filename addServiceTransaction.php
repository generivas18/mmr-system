<?php
  include("include/session_admin.php");
  require_once("include/conn.php");
  $uid = $_SESSION['admin_uid'];
  $idnum = $_GET['idnum'];

  $querycheckuser = "Select * from basic_user_info where id_number = '$idnum'";
  $qcu = mysqli_query($connection, $querycheckuser);
  $row_qcu = mysqli_fetch_assoc($qcu);
  $query_2 = "SELECT * FROM service";
  $result_2 = mysqli_query($connection, $query_2);
  if(isset($_POST["availService"])){
    $user_id = $_POST["names"];
    $service_id = $_POST["servicedata"];
    $remarks = $_POST["remarks"];
    $extrafee = $_POST["extrafee"];
    $query_13 = "SELECT * FROM services where service_id = $service_id";
    $result_13 = mysqli_query($connection, $query_13);
    $row_13 = mysqli_fetch_assoc($result_13);
    $totalfee = $row_13['fixed_rate'];

    $user_id = mysqli_real_escape_string($connection, $user_id);
    $service_id = mysqli_real_escape_string($connection, $service_id);
    $totalfee = mysqli_real_escape_string($connection, $totalfee);
    $remarks = mysqli_real_escape_string($connection, $remarks);
    $extrafee = mysqli_real_escape_string($connection, $extrafee);

    if($extrafee == ''){
        $extrafee = 0;
    }
    $genser_idquery = "Select coalesce(max(service_transaction_id), 0) + 1 as service_transaction_id from service_transactions";
    $g2 = mysqli_query($connection, $genser_idquery);
    $row_genserid = mysqli_fetch_assoc($g2);
    $genser_id = $row_genserid['service_transaction_id'];
    $query_genst= mysqli_query($connection, "INSERT INTO service_transactions (service_transaction_id, service_id, total_fee, extra_fee, user_id, admin_id, st_datetime, remarks) VALUES ($genser_id, $service_id, $totalfee, $extrafee, $user_id, $uid, CURRENT_TIMESTAMP, '$remarks')");
    if($query_genst){
        $genser_idquerylog = "Select coalesce(max(log_id), 0) + 1 as log_id from logs";
        $g3 = mysqli_query($connection, $genser_idquerylog);
        $row_genserlog = mysqli_fetch_assoc($g3);
        $genser_logid = $row_genserlog['log_id'];
        $query_ser_logs = mysqli_query($connection, "INSERT INTO logs (log_id, log_type, log_time, service_transaction_id) VALUES ($genser_logid,'service_purchase', CURRENT_TIMESTAMP,  $genser_id)");
        if($query_ser_logs){
            echo "<script type='text/javascript'>alert('Service Purchased Successfully')</script>";
            echo "<script>window.location='logs.php?tosort=all#tab4';</script>";
            echo "<script>close()</script>";
        }
    }   
 }
 
 ?>
<html>
  <head>
      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <title>SERDAC System</title>
      <link type="text/css" rel="stylesheet" href="css/stylesheet.css" />
      <link type="text/css" rel="stylesheet" href="css/materialize.css" media="screen,projection" />

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <link rel="icon" href="images/favicon.ico" type="image/ico" sizes="16x16">
      <div class="navbar-fixed">
            <nav>
                <div class="nav-wrapper blue row">
                <a href="admindashboard.php" class="brand-logo left">SERDAC</a>
                    <ul id="nav-mobile" class="right hide-on-med-and-down">
                        <a href="include/logout_admin.php">Logout</a>
                    </ul>   
                </div>
            </nav>
        </div>
  </head>
<body>

    <div class="container">
      <div class="row">        
        <div class="col s12 center-align">
              <div class="container">
                        <h4 class="left-align"><u>Avail Service</u>
                            </h4> 
                        <form action="" method="POST">    
                           
                                <div class="input-field col s12">
                                <input disabled value="<?php echo $row_qcu["firstname"]." ".$row_qcu["lastname"]." (".$row_qcu["id_number"].")"?>" id="disabled" type="text" class="validate">
                                <label for="disabled">Name</label>
                             
                            </div>
                            <input type="hidden" name="names" value="<?php echo $row_qcu["user_id"]?>"/>
                        
                        <div class ="input-field col s12">        
                            <select name="servicedata">
                                <?php 
                                     $query_ser = "SELECT * FROM services";
                                     $result_ser = mysqli_query($connection, $query_ser); 
                                    while($row_res = mysqli_fetch_assoc($result_ser)){
                                ?>
                                <option value="<?php echo $row_res['service_id'] ?>" ><?php echo $row_res['service_name']." (&#8369 ".number_format((float)$row_res['fixed_rate'], 2, '.', '').")"  ?></option><?php
                                    }
                                ?>
                            </select>
                        <label>Services</label>
                        </div>
                                <div class="input-field col s12">
                                    <input name="extrafee" id="extrafee" type="text" class="validate">
                                    <label for="extrafee">Additional Fee</label>
                                </div>
                                <div class="input-field col s12">
                                    <input name="remarks" id="remarks" type="text" class="validate">
                                    <label for="remarks">Remarks</label>
                                </div>
                    <button class="btn-large blue" type="submit" name="availService">Avail Service</button>
                    </form>
                    </div>
            </div>   
        </div>
      </div>
    </div> 

    
</body>
  <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/materialize.min.js">
    </script>
   <script>$(document).ready(function(){
    $('select').formSelect();
  });
    </script>
</html>