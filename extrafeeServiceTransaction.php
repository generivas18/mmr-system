<?php
  include("include/session_admin.php");
  require_once("include/conn.php");
  $uid = $_SESSION['admin_uid'];
  $idnum = $_GET['number'];

  $querycheckuser = "SELECT b.id_number, b.firstname, b.lastname, s.service_name, s.fixed_rate, st.extra_fee, st.total_fee FROM service_transactions st, basic_user_info b, services s WHERE b.user_id = st.user_id
   and st.service_id = s.service_id and st.service_transaction_id = $idnum";
  $qcu = mysqli_query($connection, $querycheckuser);
  $row_qcu = mysqli_fetch_assoc($qcu);
  $query_2 = "SELECT * FROM service";
  $result_2 = mysqli_query($connection, $query_2);
  $extrafee = $row_qcu["extra_fee"];
  if($extrafee>0){
    echo "<script>window.location='viewServiceTransaction?number=$idnum';</script>";
    echo "<script>close()</script>";
  }
  
  if(isset($_POST["extraRateService"])){
    $extrarate = $_POST["extrarate"];
    $totalfee = $row_qcu['fixed_rate'];

    $totalfee = mysqli_real_escape_string($connection, $totalfee);
    $extrarate = mysqli_real_escape_string($connection, $extrarate);

    $query_genst= mysqli_query($connection, "UPDATE service_transactions set extra_fee=$extrarate where service_transaction_id = $idnum");
    if($query_genst){
        $genser_idquerylog = "Select coalesce(max(log_id), 0) + 1 as log_id from logs";
        $g3 = mysqli_query($connection, $genser_idquerylog);
        $row_genserlog = mysqli_fetch_assoc($g3);
        $genser_logid = $row_genserlog['log_id'];
        $query_ser_logs = mysqli_query($connection, "INSERT INTO logs (log_id, log_type, log_time, service_transaction_id) VALUES ($genser_logid,'extra_service_charge', CURRENT_TIMESTAMP,  $idnum)");
        if($query_ser_logs){
            echo "<script type='text/javascript'>alert('Added Additional Fee')</script>";
            echo "<script>window.location='logs.php?tosort=all#tab4';</script>";
            echo "<script>close()</script>";
        }
    }   
 }
 
 ?>
<html>
  <head>
      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <title>SERDAC System</title>
      <link type="text/css" rel="stylesheet" href="css/stylesheet.css" />
      <link type="text/css" rel="stylesheet" href="css/materialize.css" media="screen,projection" />

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <link rel="icon" href="images/favicon.ico" type="image/ico" sizes="16x16">
      <div class="navbar-fixed">
            <nav>
                <div class="nav-wrapper blue row">
                <a href="admindashboard.php" class="brand-logo left">SERDAC</a>
                    <ul id="nav-mobile" class="right hide-on-med-and-down">
                        <a href="include/logout_admin.php">Logout</a>
                    </ul>   
                </div>
            </nav>
        </div>
  </head>
<body>

    <div class="container">
      <div class="row">        
        <div class="col s12 center-align">
              <div class="container">
                        <h4 class="center-align">
                        <a href="logs.php?tosort=all#tab4" class="btn-floating btn-medium blue left hide-on-med-and-down"><i class="material-icons">arrow_back</i></a>
             
                        <u> Service Extra Purchase</u>
                            </h4> 
                        
                             <input disabled value="<?php echo "Name: ".$row_qcu["firstname"]." ".$row_qcu["lastname"]?>">
                                <input disabled value="<?php echo "Service Name: ".$row_qcu["service_name"]?>" id="disabled">
                                <input disabled value="<?php echo "Fee: ".$row_qcu["total_fee"]?>" id="disabled"> 
                                <input disabled class="col s11" value="<?php echo "Additional Fee: None"?>" id="disabled"><a class="btn-floating btn-medium blue modal-trigger right hide-on-med-and-down btn tooltipped" data-position="right" data-tooltip="Add Additional Fee" href="#addService"><i class="material-icons">add</i></a>
                                <input disabled value="<?php echo "Total: ".$row_qcu["total_fee"]?>" id="disabled"> 
                   
                    <button class="btn-large blue" type="submit" name="extraRateService">Add Extra Rate</button>
                            
                    </div>
            </div>   
        </div>
      </div>
    </div> 

      <div id="addService" class="modal">
              <div class="modal-content">
              <div class="center-align">
              <div class="col s12">
              <h4>Additional Service</h4>
                <form action="" method="POST">
                <div class="row">
                                <div class="input-field col s12">
                                    <input type="text" name="extrarate" id="extrarate" class="validate">
                                    <label for="extrarate">Extra Rate</label>
                                </div> 
                </div>
                    <button class="btn-large blue" type="submit" name="extraRateService">Add Extra Rate</button>
                </form>
                </div>
                </div>
        </div>
    </div>
</body>
  <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/materialize.min.js">
    </script>
   <script>$(document).ready(function(){
    $('select').formSelect();
  });

   var elem1 = document.querySelector('#addService');
    var instance1 = M.Modal.init(elem1);

    $(document).ready(function(){
    $('.tooltipped').tooltip();
  });
    </script>
</html>